# Atividade VI - Exercícios Práticos de Aprendizagem

Esta atividade tem por finalidade utilizar todas as estruturas abordadas em nossa disciplina. Reforçando o conhecimento sobre as HTML e CSS na criação de Web Sites.

# Tutorial para criação de estrutura e estilização de uma página web.

## Primeiro Passo - Criando a estrutura elementar
### Primeiramente, vamos criar a estrutura básica para o nosso web site.
Crie o arquivo index.html na estrutura de diretórios a seguir:
* me-ensina-ai
  * images
  * estilo
  * index.html

Agora vamos construir a estrutura básica do nosso web site. Digite o código abaixo no arquivo index.html.

```html
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home | Me Ensina Aí</title>
</head>
<body>
    <header></header>
    <main></main>
    <footer></footer>
</body>
</html>
``` 

Agora que possuímos uma estrutura elementar, contendo um cabeçalho, uma área principal, e um rodapé, vamos pensar em nosso design. Vamos adicionar ao layout principal duas colunas; 
Atualize o layout adicionando duas seções, adicione o código abaixo à tag main.

```html
    <main>
        <section>

        </section>
        <section>
            
        </section>
    </main>
``` 

Logo após, iremos definir as seções, uma para o conteúdo principal e outra para conteúdo adicional. Para diferenciá-las inicialmente, vamos criar dois artigos na seção principal; atualize o seu código.

```hmtl
    <main>
        <section>

        </section>
        <section>
            <article>

            </article>
            <article>
                
            </article>
        </section>
    </main>
```

Vamos adicionar  um menu de navegação também, adicionando a tag <nav> abaixo do <header>, adicione o código abaixo.

```html
    <header></header>
    <nav></nav>
    <main>
```

Sua estrutura deve estar assim...

```html
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home | Me Ensina Aí</title>
</head>
<body>
    <header></header>
    <nav></nav>
    <main>
        <section>

        </section>
        <section>
            <article>

            </article>
            <article>

            </article>
        </section>
    </main>
    <footer></footer>
</body>
</html>
```



Atualize a estrutura HTML do menu de navegação
```html
    <nav>
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">Contato</a></li>
            <li><a href="#">Sobre mim</a></li>
        </ul>
    </nav>
```

Para finalizar, vamos colocar três divs dentro do rodapé, par dispor nosso conteúdo.
Notem que temos uma div que trabalha como um contêiner, que apenas acomoda as divs que irão compor as colunas.
A seguir temos tres divs que irão acomodar o conteúdo das colunas.
Atualize o rodapé com o código abaixo. 

```html
    <footer>
        <section>
            <div></div>
            <div></div>
            <div></div>
        </section>
    </footer>
```

## Agora a estrutura está pronta!
### Se código deve estar assim

```html
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home | Me Ensina Aí</title>
</head>
<body>
    <header></header>
    <nav>
        <ul>
            <li><a href="#"></a>Home</li>
            <li><a href="#"></a>Blog</li>
            <li><a href="#"></a>Contato</li>
            <li><a href="#"></a>Sobre mim</li>
        </ul>
    </nav>
    <main>
        <section>

        </section>
        <section>
            <article>

            </article>
            <article>

            </article>
        </section>
    </main>
    <footer>
        <section>
            <div></div>
            <div></div>
            <div></div>
        </section>
    </footer>
</body>
</html>
``` 

# Segundo Passo - Definindo o estilo

Após definitivamente definirmos a estrutura do nosso web site utilizando o HTML; agora vamos começar a definir a estilização de nossos elementos da DOM (Document Object Model) utilizando a linguagem de estilização CSS (Cascading Style Sheet).

## Criando uma folha de estilos
Até o momento, estávamos utilizando as regras da linguagem CSS internamente, dentro das Tags `<style>`.
A partir de agora iremos utilizar a forma externa de utilização do CSS. A forma externa, permite a reutilização das regras em outras páginas de seu web site.
Para a criação de uma folha de estilos que possibilite sua reutilização, sigas os passos a seguir:

1. Crie o arquivo estilo.css dentro do diretório estilo em sua árvore de diretórios.
2. Crie um link entre o arquivo index.html e a folha de estilos estilo.css:
A tag <link> cria uma ligação entre sua web page e a folha de estilo
a. Dentro da tag `<head>`, digite o código a seguir:

```html
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home | Me Ensina Aí</title>
    <link rel="stylesheet" href="estilo/estilo.css">
</head>
```

b. Defina um estilo de fonte externo a partir do Google Fonts. Você também poderá acessá-lo (google.com/fonts/) e escolher uma fonte de sua preferência:
```html
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home | Me Ensina Aí</title>
    <link rel="stylesheet" href="estilo/estilo.css">
    <!-- Fonte Customizadas via CDN -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@800&display=swap" rel="stylesheet">
</head>
```

3. Crie as regras CSS para sua folha de estilos.
# Terceiro passo - Criando as regras para estilizar as Tags em nosso index.html.

Agora vamos estilizar nossa web page, passo a passo atribuindo novos conceitos, todos eles devidamente comentados,ou seja, comente logo acima do CCS o que realmente sua formatação está realizando.

A primeira regra será remover o padding e o margin padrão do navegador. O navegador possui configurações padrão para quando nós não as especificamos. Para evitar que ele faça configurações que não desejamos, vamos retirar a sua configuração padrão com o código abaixo.

```css
* {
    margin: 0px;
    padding: 0px;
}
```

## Body
```css
body {
    background: linear-gradient(to bottom, rgba(229, 229, 229, 1) 10%, rgba(255, 255, 255, 1) 100%);
    font-family: Raleway, sans-serif, serif;
    color: #575757;
}
```

## Cabeçalho
```css
header {
    height: 135px;
    background-color: #494949;
}
```

## Título no cabeçalho
```css
header h1 {
    color: white;
    size: 48px;
    float: right;
    position: absolute;
    top: 50px;
    right: 100px;
}
```
## Menu de navegação 
```css
nav {
    background-color: #960000;
    margin-top: 1px;
    padding: 5px;
}

nav ul li {
    /* color: white; */
    display: inline;
    /*	Modificar a exibicao da lista para horizontal	*/
    margin-right: 20px;
    /*	Margem direita com 20 pixels	*/
}


/*	Efeito para mudar a cor do link, quando o cursor estiver sobre ele	*/

li a:hover {
    color: #ff7171;
    height: 25px;
}

/*	Classe para links, quando dentro dentro do menu	*/

a {
    text-decoration: none;
    /*	Remover o sublinhado de um link	 */
    color: white;
}
```

## Conteiner principal
```css
main {
    display: inline-block;
    margin: 0 5%;
}
```

## Seção Principal e Seção de conteúdo adicional
```css
/*Secao generica - Perimite especializar as classes esquerda e direita */

section {
    background-color: white;
    padding: 10px;
    margin-top: 30px;
    border-radius: 10px;
}


/* Secao especializada da generica, a esquerda */
.esquerda {
    width: 25%;
    float: left;
}


/* Secao especializada da generica, a direita */
.direita {
    width: 65%;
    float: right;
}

/* Reset para auxliar... */
.reset {
    clear: both;
}
```

## Artigos, Títulos e Títulos de Artigos
```css
/*	Formatacao dos Artigos */
article {
    background-color: white;
    border-radius: 5px;
    border: 1px dotted gray;
    padding: 10px;
    margin: 20px 20px;
    /*	Atlinhamento Justificado	*/
    text-align: justify;
}

/*	Titulos isolados e titulos dentro de um Artigo	*/
article h2 {
    /*	Borda pontilhada inferior nos titulos*/
    border-bottom: 1px solid gray;    
    margin: 5px;
    color: #960000;
}
```

## Rodapé
O rodapé possuirá três colunas para dispor o conteúdo. Precisamos modificar o modo de exibição das divs utilizadas. Poderíamos utilizar também as propriedades float para alinhá-las e a propriedade clear para a tag logo abaixo, porém vamos utilizar a propriedade display. Digite o código abaixo para o rodapé.

```css
footer {
    background-color: #dbdbdb;
    height: 150px; 
}

/*	Div de conteiner para estruturar três colunas no rodapé	*/

div.tabela {
    display: table;
    /*	Mudanca do modo padrão "block", para table	*/
    width: 100%;
    margin-top: 50px;
}

/*	Divs do conteiner para estruturar três colunas no rodapé	*/

div.celula {
    display: table-cell;
    padding: 10px;
    margin: 20px auto;    
    /*	Comprimento das colunas para preencher o rodapé	*/
    width: 33%;
    color: #960000;
}
```

## Links
Os links possuem cores padronizadas para sua exibição. O código abaixo o modifica.

```css
/*	Mudança da cor padrão dos links para cinza	*/
a {
    color: #636262;
}

/*	Mudança do efeito do cursor sobre o mouse	*/
a:hover {
    color: #960000;
}
``` 

## Terceiro passo - Especificar o CSS à estrutura
Agora para que vocês possam ver as alterações mais facilmente, vamos adicionar o CSS ao blocos estruturais da página.

1. Adicione a classe `esquerda` à primeira seção dentro do `main`.
```html
    <main>
        <section class="esquerda">

        </section>
        <section>
            
        </section>
    </main>
```
2. Adicione a classe `direita` à segunda seção dentro do `main`.
```html
    <main>
        <section class="esquerda">

        </section>
        <section class="direita">
            
        </section>
    </main>
```
3. Ajuste a estrutura do rodapé. A classe reset serve para limpar o comportamento de flutuar das tags logo acima.
```html
    <footer class="reset">
        <div class="tabela">
            <div class="celula"></div>
            <div class="celula"></div>
            <div class="celula"></div>
        </div>
    </footer>
```

### Sua página deve estar assim

```html
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home | Me Ensina Aí</title>
    <link rel="stylesheet" href="estilo/estilo.css">
</head>

<body>
    <header></header>
    <nav>
        <ul>
            <li>
                <a href="#">Home</a>
            </li>
            <li>
                <a href="#">Blog</a>
            </li>
            <li>
                <a href="#">Contato</a>
            </li>
            <li>
                <a href="#">Sobre mim</a>
            </li>
        </ul>
    </nav>
    <main>
        <section class="esquerda">
            <article>

            </article>
        </section>
        <section class="direita">
            <article>

            </article>

            <article>
            </article>
        </section>
    </main>
    </div>
    <footer class="reset">
        <div class="tabela">
            <div class="celula"></div>
            <div class="celula"></div>
            <div class="celula"></div>
        </div>
    </footer>
</body>

</html>

```

## Quarto passo - Adicionando o conteúdo à sua página web

A estrutura e a estilização de nossa página web está concluída. Agora precisamos adicionar o conteúdo à ela.
Crie os conteúdos nas páginas indicas nos links do menu, não precisa se preocupar com o conteúdo no momento, utilizem os geradores de texto `Lorem Ipsum...`.

Procure alterar o CSS à sua preferencia e reutilizá-lo nas demais páginas.

# Desafio

Adicione um logotipo no canto superior esquerdo do `header` da página e coloque o título do seu site `Me Ensina Aí: Churasco` por exemplo centralizado com uma fonte que chame a atenção dos usuários!

# Sucesso!!!
